import 'package:flutter/material.dart';
import 'package:app_service/widgets/header.dart';
import 'package:app_service/widgets/menu.dart';
import 'package:app_service/widgets/tabs.dart';
import 'dart:io';
import 'constants.dart';



class AppointmentsProgress extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AppointmentsProgressState();
  }
}

class _AppointmentsProgressState extends State<AppointmentsProgress> {
  final List<Map> appointments = [
    {
      'date': 'Fecha de solicitud 8 jun 2021',
      'name':'Dr. Jhon Mendez',
      'speciality':'MEDICINA GENERAL - CARDIOLOGIA',
      'company':'Company',
      'phone': '3118352833'
    },
    {
      'date': 'Fecha de solicitud 8 jun 2021',
      'name':'Dr. Jhon Mendez',
      'speciality':'MEDICINA GENERAL - CARDIOLOGIA',
      'company':'Company',
      'phone': '3118352833'
    },
  ];
  String teamName = '';
  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: Header(),
        body: Column(
                children: <Widget>[
                  SimpleDialog(
                    title: Text(
                      "Solicitud exitosa",
                    ),
                    children: <Widget>[
                      Text(
                        "Cita cancelada correctamente",
                        textAlign: TextAlign.center,
                      ),
                    ],
                    backgroundColor: COLOR_PRIMARY,
                    elevation: 4,
                  ),
              Expanded(
                child: ListView.builder(
                    itemCount: appointments.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
                            color: COLOR_LIGHT_GREY,
                          ),
                          width: double.infinity,
                          height: 140,
                          margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                          child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Center(
                                  child: Container(
                                      width: 70,
                                      height: 70,
                                      margin: EdgeInsets.only(right: 15),
                                      child: Image(image: AssetImage("assets/images/sharing-no-image.png"))),
                                ),
                                Expanded(
                                    child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            appointments[index]['date'],
                                          ),
                                          Text(
                                            appointments[index]['name'],
                                          ),
                                          Text(appointments[index]['speciality'],),
                                          Row(
                                            children: <Widget>[
                                              Icon(Icons.call,size: 20,),
                                              SizedBox(width: 5,),
                                              Text(appointments[index]['phone'],),
                                            ],
                                          ),
                                          ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                                primary: COLOR_PRIMARY
                                            ),
                                            child: Text(
                                              'Cancelar solicitud',
                                              style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white,
                                              ),
                                            ),
                                            onPressed: () async {
                                              //Navigator.pushNamed(context, '/request-appointment');
                                              final String currentTeam = await _asyncInputDialog(context);
                                              print("Current team name is $currentTeam");
                                            },
                                          ),
                                        ])
                                )
                              ]
                          ));
                    }))
                ]),


        drawer: Menu(),
        bottomNavigationBar: Tabs()
    );
  }

  Future _asyncInputDialog(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // dialog is dismissible with a tap on the barrier
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('¿Estás seguro de cancelar la solicitud?'),
          content: new Row(
            children: [
              new Expanded(
                  child: new TextField(
                    autofocus: true,
                    decoration: new InputDecoration(
                        labelText: 'Motivo de la cancelación'),
                    onChanged: (value) {
                      teamName = value;
                    },
                  ))
            ],
          ),
          actions: [
            FlatButton(
              child: Text('Cerrar'),
              onPressed: () {
                Navigator.of(context).pop(teamName);
              },
            ),
            FlatButton(
              child: Text('Cancelar solicitud'),
              onPressed: () {
                Navigator.of(context).pop(teamName);
              },
            ),
          ],
        );
      },
    );
  }

}