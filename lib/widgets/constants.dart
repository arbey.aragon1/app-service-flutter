
import 'package:flutter/material.dart';

const COLOR_PRIMARY = Color.fromRGBO(14, 196, 173, 1.0);
const COLOR_PRIMARY_BLUE = Color.fromRGBO(0, 255, 255, 1.0);
const COLOR_LIGHT_GREY = Color.fromRGBO(248, 249,250, 1.0);
const FACEBOOK_BUTTON_COLOR = Color(0xFF415893);
