import 'package:flutter/material.dart';
import 'package:app_service/services/authService.dart';

import 'constants.dart';
import 'menu.dart';

class TermsConditions extends StatelessWidget{

  AuthService authService = AuthService();

  TermsConditions();
  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: AppBar(
            backgroundColor: Color.fromRGBO(255, 255, 255, 1.0),
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Text("Terms Conditions"),
            centerTitle: true,
        ),
        body: Center(
            child: Padding(
              padding: const EdgeInsets.only(right: 40.0, left: 40.0, top: 60),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: double.infinity),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: COLOR_PRIMARY,
                    padding: EdgeInsets.only(top: 12, bottom: 17),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: BorderSide(color: COLOR_PRIMARY)),
                  ),
                  child: Text(
                    'Ver pdf',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () async {
                    final url =
                        'https://www.adobe.com/support/products/enterprise/knowledgecenter/media/c4611_sample_explain.pdf';
                    final file = await authService.loadNetwork(url);
                  },
                ),
              ),
            ),
        ),
        drawer: Menu(),
    );
  }

}