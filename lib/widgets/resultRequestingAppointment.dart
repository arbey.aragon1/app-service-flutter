
import 'package:flutter/material.dart';
import 'package:app_service/widgets/constants.dart';
import 'package:app_service/widgets/header.dart';

class ResultRequestingAppointment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ResultRequestingAppointmentState();
  }
}

class _ResultRequestingAppointmentState extends State<ResultRequestingAppointment> {
  final Map doctor = {
      'name':'Dr. Jhon Mendez', 
      'speciality':'MEDICINA GENERAL - CARDIOLOGIA', 
      'company':'Company',
      'phone': '3118352833',
      'address': 'Calle 93 # 18 98',
      'detail': 'Torre2 - BOGOTA D.C. - USAQUEN'
      };
  
  _ResultRequestingAppointmentState();
  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: Header(),
        body: ListView(
            children: [
              SimpleDialog(
                title: Text(
                  "Solicitud exitosa",
                ),
                children: <Widget>[
                  Text(
                    "Pronto desde el consultorio de {{name}} se estara comunicando contigo",
                    textAlign: TextAlign.center,
                  ),
                ],
                backgroundColor: COLOR_PRIMARY,
                elevation: 4,
              ),
              SizedBox(height: 15,),
              Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
                    color: COLOR_LIGHT_GREY,
                  ),
                  width: double.infinity,
                  height: 120,
                  margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Center(
                          child: Container(
                              width: 70,
                              height: 70,
                              margin: EdgeInsets.only(right: 15),
                              child: Image(image: AssetImage("assets/images/sharing-no-image.png"))),
                        ),
                        Expanded(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    doctor['name'],
                                  ),
                                  Text(doctor['speciality'],),
                                  Text(doctor['phone'],),
                                  Text(doctor['address'],),
                                  Text(doctor['detail'],),

                                ])
                        )
                      ]
                  ))
            ]
          )
        );
      }
    }