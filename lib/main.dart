import 'package:flutter/material.dart';
import 'package:app_service/widgets/appointmentDetail.dart';
import 'package:app_service/widgets/appointmentsProgress.dart';
import 'package:app_service/widgets/doctorList.dart';
import 'package:app_service/widgets/doctorSearch.dart';
import 'package:app_service/widgets/fileDetail.dart';
import 'package:app_service/widgets/fileList.dart';
import 'package:app_service/widgets/home.dart';
import 'package:app_service/widgets/loadFiles.dart';
import 'package:app_service/widgets/login.dart';
import 'package:app_service/widgets/patientRecord.dart';
import 'package:app_service/widgets/profile.dart';
import 'package:app_service/widgets/recentAppointments.dart';
import 'package:app_service/widgets/requestMedicalAppointment.dart';
import 'package:app_service/widgets/resultRequestingAppointment.dart';
import 'package:app_service/widgets/sharedData.dart';
import 'package:app_service/widgets/termsConditions.dart';
import 'widgets/registration.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MD Wallet',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        '/home': (context)=>Home(),
        '/patient-record':(context)=>PatientRecord(),
        '/terms-conditions':(context)=>TermsConditions(),
        '/profile':(context)=>Profile(),
        '/login':(context)=>LoginScreen(),
        '/register':(context)=>Registration(),
        '/recent-appointments':(context)=>RecentAppointments(),
        '/doctor-serch':(context)=>DoctorSearch(),
        '/doctor-list': (context)=>DoctorList(),
        '/request-appointment': (context)=>RequestAppointment(),
        '/result-requesting-appointment': (context)=>ResultRequestingAppointment(),
        '/load-files': (context)=>LoadFiles(),
        '/file-list': (context)=>FileList(),
        '/file-detail': (context)=>FileDetail(),
        '/appointments-progress': (context)=>AppointmentsProgress(),
        '/shared-data': (context)=>SharedData(),
        '/appointment-detail': (context)=>AppointmentDetail()
      },
      initialRoute: '/login',
    );
  }
}
