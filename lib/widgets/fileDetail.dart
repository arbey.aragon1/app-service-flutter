import 'package:flutter/material.dart';
import 'package:app_service/widgets/header.dart';
import 'package:app_service/widgets/menu.dart';
import 'package:app_service/widgets/tabs.dart';
import 'dart:io';
import 'constants.dart';

class FileDetail extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FileDetailState();
  }
}

class _FileDetailState extends State<FileDetail> {
  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: Header(),
        body: Container(
          padding: EdgeInsets.only(left: 16, top: 25, right: 16 ),
          child: ListView(
            children: [
              Text("Titulo", style: TextStyle(
                fontSize: 25, fontWeight: FontWeight.w500
              )),
              SizedBox(height: 15,),
              Text("Date", style: TextStyle(
                fontSize: 25, fontWeight: FontWeight.w500
              )),
              SizedBox(height: 15,),
              Center(child: 
                Stack(children: [
                  Container(
                    width: 300,
                    height: 300,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: new AssetImage("assets/images/radiografia.jpeg")
                      )
                    ),
                  )
                  ],
                ),
              ),
              Text("Comentarios", style: TextStyle(
                  fontSize: 20, fontWeight: FontWeight.w500
              )),
              SizedBox(height: 15,),
              ]
            )
        ),
        drawer: Menu(),
        bottomNavigationBar: Tabs()
    );
  }
}