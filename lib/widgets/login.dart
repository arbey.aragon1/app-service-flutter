import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:app_service/services/authService.dart';

import '../main.dart';
import 'constants.dart';
import 'header.dart';
import 'helper.dart';
import 'dart:developer';

class LoginScreen extends StatefulWidget {
  @override
  State createState() {
    return _LoginScreenStatus();
  }
}

class _LoginScreenStatus extends State<LoginScreen> {
  GlobalKey<FormState> _key = new GlobalKey();
  AutovalidateMode _validate = AutovalidateMode.disabled;
  String? email, password;
  AuthService authService = AuthService();

  _LoginScreenStatus();
  @override
  Widget build(BuildContext context){
        return Scaffold(
            body: Form(
            key: _key,
            autovalidateMode: _validate,
            child: ListView(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(30),
                  //padding: EdgeInsets.only(left: 30, right: 30),
                  child: Center(
                    child: Image.asset(
                        "assets/images/md-wallet-logo.png",
                        height: 200,
                    ),
                  ),
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(minWidth: double.infinity),
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 32.0, right: 40.0, left: 40.0),
                    child: TextFormField(
                        textAlignVertical: TextAlignVertical.center,
                        textInputAction: TextInputAction.next,
                        validator: validateEmail,
                        onSaved: (val) => email = val,
                        onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
                        style: TextStyle(fontSize: 15.0),
                        keyboardType: TextInputType.emailAddress,
                        cursorColor: COLOR_PRIMARY,
                        decoration: InputDecoration(
                            contentPadding:
                                new EdgeInsets.only(left: 16, right: 16),
                            fillColor: Colors.white,
                            hintText: 'Email Address',
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                    color: COLOR_PRIMARY, width: 2.0)),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ))),
                  ),
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(minWidth: double.infinity),
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 15.0, right: 40.0, left: 40.0),
                    child: TextFormField(
                        textAlignVertical: TextAlignVertical.center,
                        validator: validatePassword,
                        onSaved: (val) => password = val,
                        onFieldSubmitted: (password) async {
                          //await login();
                        },
                        obscureText: true,
                        textInputAction: TextInputAction.done,
                        style: TextStyle(fontSize: 15.0),
                        cursorColor: COLOR_PRIMARY,
                        decoration: InputDecoration(
                            contentPadding:
                                new EdgeInsets.only(left: 16, right: 16),
                            fillColor: Colors.white,
                            hintText: 'Password',
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                    color: COLOR_PRIMARY, width: 2.0)),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 40.0, left: 40.0, top: 60),
                  child: ConstrainedBox(
                    constraints: const BoxConstraints(minWidth: double.infinity),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: COLOR_PRIMARY,
                    padding: EdgeInsets.only(top: 12, bottom: 17),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: BorderSide(color: COLOR_PRIMARY)),
                  ),
                  child: Text(
                    'Iniciar sesión',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () {
                    login();
                      Navigator.pushNamed(context, '/home');
                    },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 40.0, left: 40.0, top: 15),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: double.infinity),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary:  COLOR_PRIMARY,
                    padding: EdgeInsets.only(top: 12, bottom: 17),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: BorderSide(color: COLOR_PRIMARY)),
                  ),
                  child: Text(
                    'Nuevo registro',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: (){
                      Navigator.pushNamed(context, '/register');
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  
  login(){
    print('--data: ');
    authService.loginUser("jhon@gmail.com","123456789", (){});
  }
}
