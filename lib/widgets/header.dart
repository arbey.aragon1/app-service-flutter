import 'package:flutter/material.dart';

class Header extends StatelessWidget implements PreferredSizeWidget {
  final IconData icono = Icons.notifications_none;
  Header();
  @override
  Widget build(BuildContext context){
    return AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          title: Image.asset("assets/images/md-wallet-logo.png",
            height: 50,
            width: 50,
            fit: BoxFit.contain,
            alignment: Alignment.topLeft,
          ),
          backgroundColor: Color.fromRGBO(255, 255, 255, 1.0),
          actions: <Widget>[
            IconButton(
              padding: EdgeInsets.all(10.0),
              icon: Icon(Icons.notifications_none,
              color: Colors.black),
              onPressed: () => {
                Navigator.pushNamed(context, '/appointments-progress')
              }
            ),
            InkWell(
              onTap: () {
                  Navigator.pushNamed(context, '/profile');
              },
              child: Container(
                  margin: EdgeInsets.all(5),
                  padding: EdgeInsets.only(left: 30, right: 30),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: Image.asset(
                        "assets/images/sharing-no-image.png"),
                    ),
                  )
            )
          ],
        );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

}