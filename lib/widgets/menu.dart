import 'package:flutter/material.dart';

class Menu extends StatelessWidget {
  Menu();
  @override
  Widget build(BuildContext context){
    return Drawer(
          child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(



              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(70),
                    child: Image.asset(
                      "assets/images/sharing-no-image.png"),
                  ),
                  

                  Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                   
                      children: <Widget>[
                        Text('Luisa Pink', 
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 17,
                            fontWeight: FontWeight.w700,
                            
                          )
                        
                        ),
                        Text('Usuario',
                          style: TextStyle(
                            color: Color.fromRGBO(14, 196, 173, 1.0), 
                            fontSize: 17,
                            fontWeight: FontWeight.w700,
                           
                         
                        )),
                      ],
                      ),
                    )
                 
                ],
              ),



              
            ),
            ListTile(
              title: Text('Citas recientes'),
              onTap: () {
                Navigator.pushNamed(context, '/recent-appointments');
              },
              leading: Icon(
                    Icons.event_available,
                    size: 26.0,
                    color: Colors.black,
                  ),
            ),
            ListTile(
              title: Text('Citas programadas'),
              onTap: () {
                Navigator.pushNamed(context, '/appointments-progress');
              },
              leading: Icon(
                Icons.date_range,
                size: 26.0,
                color: Colors.black,
              ),
            ),
            ListTile(
              title: Text('Buscar médico'),
              onTap: () {
                Navigator.pushNamed(context, '/doctor-serch');
              },
              leading: Icon(
                Icons.saved_search,
                size: 26.0,
                color: Colors.black,
              ),
            ),
            ListTile(
              title: Text('Cargar archivos'),
              onTap: () {
                Navigator.pushNamed(context, '/load-files');
              },
              leading: Icon(
                Icons.upload_file,
                size: 26.0,
                color: Colors.black,
              ),
            ),
            ListTile(
              title: Text('Mis archivos'),
              onTap: () {
                Navigator.pushNamed(context, '/file-list');
              },
              leading: Icon(
                Icons.snippet_folder,
                size: 26.0,
                color: Colors.black,
              ),
            ),
            ListTile(
              title: Text('Compartir archivos'),
              onTap: () {
                Navigator.pushNamed(context, '/shared-data');
              },
              leading: Icon(
                Icons.share,
                size: 26.0,
                color: Colors.black,
              ),
            ),
            ListTile(
              title: Text('Cerrar sesíon'),
              onTap: () {
                Navigator.pushNamed(context, '/login');
              },
              leading: Icon(
                Icons.power_settings_new,
                size: 26.0,
                color: Colors.black,
              ),
            ),
          ],
        ),
      );
        
  }
}