import 'package:flutter/material.dart';
import 'package:app_service/widgets/header.dart';
import 'package:app_service/widgets/menu.dart';
import 'package:app_service/widgets/tabs.dart';
import 'dart:io';
import 'constants.dart';

class FileItem
{
  String date, title;
  FileItem(this.date, this.title);
}


class FileList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FileListState();
  }
}

class _FileListState extends State<FileList> {

  late List fileList;

  void initState()
  {
    fileList = [
      FileItem('8. Jun. 2020', 'file name' ),
      FileItem('4. Jun. 2020', 'file name' ),
      FileItem('10. Jun. 2020', 'file name'),
      FileItem('11. Jun. 2020', 'file name'),
      FileItem('14. Jun. 2020', 'file name'),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: Header(),
        body: Container(
            padding: EdgeInsets.only(left: 16, top: 25, right: 16 ),
            child:ListView.builder(
              itemCount: fileList.length,
              itemBuilder: (context, index) {
                return Card(
                  child: ListTile(
                      title: Text('${fileList[index].title}'),
                      subtitle: Stack(children: [
                        Container(
                            child: Text(
                                '${fileList[index].date}'
                            )
                        ),
                      ]),
                      onTap: () {
                        Navigator.pushNamed(context, '/file-detail');
                      }
                  ),
                );
              },
            ),
        ),
        drawer: Menu(),
        bottomNavigationBar: Tabs()
    );
  }
}