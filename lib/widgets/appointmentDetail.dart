import 'package:flutter/material.dart';
import 'package:app_service/widgets/header.dart';
import 'package:app_service/widgets/menu.dart';
import 'package:app_service/widgets/tabs.dart';

import 'constants.dart';

class AppointmentDetail extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AppointmentDetailState();
  }
}

class _AppointmentDetailState extends State<AppointmentDetail> {
  final Map appointment = {
    'name': 'Dr. Jhon Mendez',
    'speciality': 'MEDICINA GENERAL - CARDIOLOGIA',
    'company': 'Company',
    'phone': '3118352833',
    'address': 'Calle 93 # 18 98',
    'detail': 'Torre2 - BOGOTA D.C. - USAQUEN',
    'status': 'Consulta de primera vez'
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: Header(),
        drawer: Menu(),
        body: Container(
            padding: EdgeInsets.only(left: 16, top: 25, right: 16),
            child: ListView(children: [
              Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10)),
                    color: COLOR_LIGHT_GREY,
                  ),
                  width: double.infinity,
                  height: 120,
                  margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Center(
                          child: Container(
                              width: 70,
                              height: 70,
                              margin: EdgeInsets.only(right: 15),
                              child: Image(
                                  image: AssetImage(
                                      "assets/images/sharing-no-image.png"))),
                        ),
                        Expanded(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                              Text(
                                appointment['status'],
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 10,
                                    background: Paint()
                                      ..strokeWidth = 14.0
                                      ..color = COLOR_PRIMARY
                                      ..style = PaintingStyle.stroke
                                      ..strokeJoin = StrokeJoin.round),
                              ), SizedBox(height: 15,),
                              Text(
                                appointment['name'],
                              ),
                              Text(
                                appointment['speciality'],
                              ),

                            ])),
                      ])),
              Row(children: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: COLOR_PRIMARY
                  ),
                  child: Text(
                    'Ver órdenes',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () async {
                    //Navigator.pushNamed(context, '/request-appointment');
                  },
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: COLOR_PRIMARY
                  ),
                  child: Text(
                    'Ver historia',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () async {
                    //Navigator.pushNamed(context, '/request-appointment');
                  },
                ),
              ],),

              Row(children: [
                Icon(
                    Icons.phone,
                    size: 26.0,
                    color: Colors.black,
                  ),
                  Text(
                    appointment['phone'],
                  ),
              ],),
              Row(children: [
                Icon(
                    Icons.directions,
                    size: 26.0,
                    color: Colors.black,
                  ),
                  Text(
                    appointment['address'],
                  ),
              ],),
              Row(children: [
                Icon(
                    Icons.build,
                    size: 26.0,
                    color: Colors.black,
                  ),
                  Text(
                    appointment['detail'],
                  ),
              ],),
              
            ])),
        bottomNavigationBar: Tabs());
  }
}
