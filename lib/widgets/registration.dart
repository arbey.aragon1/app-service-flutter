import 'package:flutter/material.dart';
import 'package:app_service/services/authService.dart';

import 'constants.dart';
import 'helper.dart';

class Registration extends StatefulWidget {
  @override
  State createState() {
    return _Registration();
  }
}

class _Registration extends State<Registration> {
    GlobalKey<FormState> _key = new GlobalKey();
  AutovalidateMode _validate = AutovalidateMode.disabled;
  String? email, password;
  bool isChecked = false;

  AuthService authService = AuthService();
  _Registration();
  @override
  Widget build(BuildContext context){
        return Scaffold(
            body: Form(
            key: _key,
            autovalidateMode: _validate,
            child: ListView(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(30),
                  //padding: EdgeInsets.only(left: 30, right: 30),
                  child: Center(
                    child: Image.asset(
                        "assets/images/md-wallet-logo.png",
                        height: 200,
                    ),
                    
                  ),
                  
                ),
                 Center(child:Container(   
                   padding: new EdgeInsets.only(top: 40.0),
                    child: Text(
                        "Crea tu cuenta",
                        style: TextStyle(
                          fontFamily: 'Hind',
                          fontSize:18.0,
                          color: Color.fromRGBO(0, 0, 0, 0.7),
                         
                        ),
                        
                    ),
                  ),
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(minWidth: double.infinity),
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 32.0, right: 40.0, left: 40.0),
                    child: TextFormField(
                        textAlignVertical: TextAlignVertical.center,
                        textInputAction: TextInputAction.next,
                        validator: validateEmail,
                        onSaved: (val) => email = val,
                        onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
                        style: TextStyle(fontSize: 15.0),
                        keyboardType: TextInputType.emailAddress,
                        cursorColor: COLOR_PRIMARY,
                        decoration: InputDecoration(
                            contentPadding:
                                new EdgeInsets.only(left: 16, right: 16),
                            fillColor: Colors.white,
                            hintText: 'Email*',
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                    color: COLOR_PRIMARY, width: 2.0)),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ))),
                  ),
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(minWidth: double.infinity),
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 15.0, right: 40.0, left: 40.0),
                    child: TextFormField(
                        textAlignVertical: TextAlignVertical.center,
                        validator: validatePassword,
                        onSaved: (val) => password = val,
                        onFieldSubmitted: (password) async {
                          //await login();
                        },
                        obscureText: true,
                        textInputAction: TextInputAction.done,
                        style: TextStyle(fontSize: 15.0),
                        cursorColor: COLOR_PRIMARY,
                        decoration: InputDecoration(
                            contentPadding:
                                new EdgeInsets.only(left: 16, right: 16),
                            fillColor: Colors.white,
                            hintText: 'Contraseña (minimo 6 caracteres)*',
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                    color: COLOR_PRIMARY, width: 2.0)),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ))),
                  ),
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(minWidth: double.infinity),
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 15.0, right: 40.0, left: 40.0),
                    child: TextFormField(
                        textAlignVertical: TextAlignVertical.center,
                        validator: validatePassword,
                        onSaved: (val) => password = val,
                        onFieldSubmitted: (password) async {
                          //await login();
                        },
                        obscureText: true,
                        textInputAction: TextInputAction.done,
                        style: TextStyle(fontSize: 15.0),
                        cursorColor: COLOR_PRIMARY,
                        decoration: InputDecoration(
                            contentPadding:
                                new EdgeInsets.only(left: 16, right: 16),
                            fillColor: Colors.white,
                            hintText: 'Confirmar contraseña*',
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0),
                                borderSide: BorderSide(
                                    color: COLOR_PRIMARY, width: 2.0)),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 40.0, left: 40.0, top: 40),
                  child: Row(
                    children: <Widget>[
                      Checkbox(
                        value: isChecked,
                        onChanged: (bool? value) {
                          setState(() {
                            isChecked = value!;
                          });
                        },
                      ),
                      GestureDetector(
                        onTap: () async {
                          final url =
                              'https://www.adobe.com/support/products/enterprise/knowledgecenter/media/c4611_sample_explain.pdf';
                          final file = await authService.loadNetwork(url);
                        },
                        child: const Text(
                          'Acepto términos y condiciones',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 40.0, left: 40.0, top: 25),
                  child: ConstrainedBox(
                    constraints: const BoxConstraints(minWidth: double.infinity),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: COLOR_PRIMARY,
                    padding: EdgeInsets.only(top: 12, bottom: 17),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: BorderSide(color: COLOR_PRIMARY)),
                  ),
                  child: Text(
                    'Registrarse',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () {
                    //login()
                      Navigator.pushNamed(context, '/home');
                    },
                ),
              ),
            ),
                Padding(
                  padding: const EdgeInsets.only(right: 40.0, left: 40.0, top: 15),
                  child: ConstrainedBox(
                    constraints: const BoxConstraints(minWidth: double.infinity),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: COLOR_PRIMARY,
                        padding: EdgeInsets.only(top: 12, bottom: 17),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                            side: BorderSide(color: COLOR_PRIMARY)),
                      ),
                      child: Text(
                        'Regresar a login',
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () {
                        //login()
                        Navigator.pushNamed(context, '/login');
                      },
                    ),
                  ),
                ),
          ],
        ),
      ),
    );
  }



}
