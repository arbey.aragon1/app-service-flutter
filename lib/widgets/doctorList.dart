import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_service/widgets/tabs.dart';

import 'constants.dart';
import 'header.dart';
import 'menu.dart';


class DoctorList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DoctorListState();
  }
}

class _DoctorListState extends State<DoctorList> {
  final List<Map> doctors = [
    {
      'name':'Dr. Jhon Mendez', 
      'speciality':'MEDICINA GENERAL - CARDIOLOGIA', 
      'company':'Company',
      'phone': '3118352833',
      'address': 'Calle 93 # 18 98',
      'detail': 'Torre2 - BOGOTA D.C. - USAQUEN'
      },
      {
      'name':'Dr. Jhon Mendez', 
      'speciality':'MEDICINA GENERAL - CARDIOLOGIA', 
      'company':'Company',
      'phone': '3118352833',
      'address': 'Calle 93 # 18 98',
      'detail': 'Torre2 - BOGOTA D.C. - USAQUEN'
      },
  ];

  String? _selectSpecialty;
  String? _selectCity;
  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: Header(),
        body: Container(
          padding: EdgeInsets.only(left: 16, top: 25, right: 16 ),
          child: ListView.builder(
            itemCount: doctors.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
                        color: COLOR_LIGHT_GREY,
                      ),
                      width: double.infinity,
                      height: 170,
                      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Container(
                                  width: 70,
                                  height: 70,
                                  margin: EdgeInsets.only(right: 15),
                                  child: Image(image: AssetImage("assets/images/sharing-no-image.png"))),
                            ),
                            Expanded(
                              child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  doctors[index]['name'],
                                ),
                                Text(doctors[index]['speciality'],),
                                Text(doctors[index]['phone'],),
                                Text(doctors[index]['address'],),
                                Text(doctors[index]['detail'],),
                                ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        primary: COLOR_PRIMARY
                                      ),
                                      child: Text(
                                        'Solicitar cita',
                                        style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                        ),
                                      ),
                                      onPressed: () {
                                        Navigator.pushNamed(context, '/request-appointment');
                                      },
                                    ),
                            ])
                            )
                            ]
                      ));
            }),
        ),
        drawer: Menu(),
        bottomNavigationBar: Tabs()
      );
  }
}

