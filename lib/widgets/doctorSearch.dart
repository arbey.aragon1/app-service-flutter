import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_service/widgets/tabs.dart';

import 'constants.dart';
import 'header.dart';
import 'menu.dart';


class DoctorSearch extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DoctorSearchState();
  }
}

class _DoctorSearchState extends State<DoctorSearch> {
  String? _selectSpecialty;
  String? _selectCity;
  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: Header(),
        body: Container(
          padding: EdgeInsets.only(left: 16, top: 25, right: 16 ),
          child: ListView(
            children: [
              Center(child: Text("Buscar especialista y solicitar una cita", style: TextStyle(
                fontSize: 25, fontWeight: FontWeight.w500
              ))),

              DropdownButton<String>(
                isExpanded: true,
                  focusColor:Colors.white,
                  value: _selectSpecialty,
                  style: TextStyle(color: Colors.white),
                  iconEnabledColor:Colors.black,
                  items: <String>[
                    '--',
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value,style:TextStyle(color:Colors.black),),
                    );
                  }).toList(),
                  hint:Text(
                    "Seleccionar especialidad",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.w500),
                  ),
                  onChanged: (String? value) {
                    if(value!= null){
                      setState(() {
                        _selectSpecialty = value;
                      });
                    }  
                  },
                ),

              DropdownButton<String>(
                isExpanded: true,
                  focusColor:Colors.white,
                  value: _selectCity,
                  style: TextStyle(color: Colors.white),
                  iconEnabledColor:Colors.black,
                  items: <String>[
                    '--',
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value,style:TextStyle(color:Colors.black),),
                    );
                  }).toList(),
                  hint:Text(
                    "Seleccionar ciudad",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.w500),
                  ),
                  onChanged: (String? value) {
                    if(value!= null){
                      setState(() {
                        _selectCity = value;
                      });
                    }  
                  },
                ),
              Padding(
                  padding: const EdgeInsets.only(right: 40.0, left: 40.0, top: 40),
                  child: ConstrainedBox(
                    constraints: const BoxConstraints(minWidth: double.infinity),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: COLOR_PRIMARY,
                    padding: EdgeInsets.only(top: 12, bottom: 12),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: BorderSide(color: COLOR_PRIMARY)),
                  ),
                  child: Text(
                    'Buscar',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/doctor-list');},
                  ),
              ),
            ),
            ]
          )
        ),
        drawer: Menu(),
        bottomNavigationBar: Tabs()
      );
  }
}

