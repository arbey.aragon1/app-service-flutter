import 'dart:convert';
import 'dart:io';
import 'package:app_service/models/loginResponse.dart';
import 'package:http/http.dart' as http;
import 'package:app_service/services/appExceptions.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

import 'apiBaseHelper.dart';

class AuthService {
  static final AuthService _instance = AuthService._internal();
  final String url_wallet = '';
  final String url_mdbook = '';

  factory AuthService() {
    return _instance;
  }

  AuthService._internal();

  Future<LoginResponse?> loginUser(
      String username, String password, Function f1) async {
    var responseJson;
    try {
      print("------------------------1");
      //const String endpoint = "wallet.mdscitech.io";
      const String endpoint = "b6e990c3c7cc.ngrok.io";
      const String api = "login_check";
      Uri url = Uri.https(endpoint, api);

      print("------------------------2");
      print(username);
      print(password);

      final response = await http.post(
        url,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
        },
        body: json.encode(
            <String, String>{"username": username, "password": password}),
      );

      print("------------------------3");
      responseJson = returnResponse(response);

      print("------------------------4");

      /*if (response.statusCode == 200) {
        print("200");
        print(response.body);
      } else {
        throw Exception('Failed to create album.');
      }*/

      //print("------------------------4");
    } on SocketException {
      throw FetchDataException('No internet connection');
    } catch (error) {
      print("------------------------4");
      print(error);
    }
    print(responseJson);

    return LoginResponse.fromJson(json.decode(responseJson));
  }

  Future<void> setUser(
      String document,
      String email,
      String pass,
      String role,
      String firstname,
      String middlename,
      String lastname,
      String seclastname,
      Function f1) async {
    String url = url_wallet;
    String endpoint = "/api/setUser";
    var body = json.encode(<String, String>{
      "document": document,
      "email": email,
      "pass": pass,
      "role": role,
      "firstname": firstname,
      "middlename": middlename,
      "lastname": lastname,
      "seclastname": seclastname,
    });
    print(body);
  }

  Future<void> setDocument(
      String shortName, String docName, String type, Function f1) async {
    String url = url_wallet;
    String endpoint = "/api/setDocument";
    var body = json.encode(<String, String>{
      "shortName": shortName,
      "docName": docName,
      "type": type,
    });
    print(body);
  }

  Future<void> setGender(
      String shortName, String genderName, Function f1) async {
    String url = url_wallet;
    String endpoint = "/api/setGender";
    var body = json.encode(<String, String>{
      "shortName": shortName,
      "genderName": genderName,
    });
    print(body);
  }

  Future<void> getFiles(String document, String docid, Function f1) async {
    String url = url_wallet;
    String endpoint = "/api/getFiles";
    var body = json.encode(<String, String>{
      "document": document,
      "docid": docid,
    });
    print(body);
  }

  Future<void> cancelBooking(
      String idbooking, String cancelationreason, Function f1) async {
    String url = url_mdbook;
    String endpoint = "/api/Appointments/cancelBooking";
    var body = json.encode(<String, String>{
      "idbooking": idbooking,
      "cancelationreason": cancelationreason,
    });
    print(body);
  }

  Future<void> setBooking(
      String bookingdate,
      String patientdoc,
      String email,
      String phone,
      String firstname,
      String middlename,
      String lastname,
      String seclastname,
      String doctype,
      String bkdoctor,
      String bktenant,
      String bookingcomment,
      Function f1) async {
    String url = url_mdbook;
    String endpoint = "/api/set/booking";
    var body = json.encode(<String, String>{
      "bookingdate": bookingdate,
      "patientdoc": patientdoc,
      "email": email,
      "phone": phone,
      "firstname": firstname,
      "middlename": middlename,
      "lastname": lastname,
      "seclastname": seclastname,
      "doctype": doctype,
      "bkdoctor": bkdoctor,
      "bktenant": bktenant,
      "bookingcomment": bookingcomment,
    });
    print(body);
  }

  Future<void> setPatient(
      String document,
      String firstname,
      String middlename,
      String lastname,
      String seclastname,
      String dob,
      String cellphone,
      String email,
      String doctype,
      String sex,
      String drid,
      Function f1) async {
    String url = url_mdbook;
    String endpoint = "/api/set/patient";
    var body = json.encode(<String, String>{
      "document": document,
      "firstname": firstname,
      "middlename": middlename,
      "lastname": lastname,
      "seclastname": seclastname,
      "dob": dob,
      "cellphone": cellphone,
      "email": email,
      "doctype": doctype,
      "sex": sex,
      "drid": drid,
    });
    print(body);
  }

  Future<void> getPendingPatientAppointments(
      String document, Function f1) async {
    String url = url_mdbook;
    String endpoint = "/api/Appointments/NextByPatient/document";
    print(url + endpoint);
  }

  Future<void> getAllPendingAppointments(String document, Function f1) async {
    String url = url_mdbook;
    String endpoint = "/api/Appointments/bookingProgress/document";
    print(url + endpoint);
  }

  Future<void> getAllSpecialties(Function f1) async {
    String url = url_mdbook;
    String endpoint = "/api/getAllSpecialties";
    print(url + endpoint);
  }

  Future<void> getPendingAppointments(
      String idCity, String specialityname, Function f1) async {
    String url = url_mdbook;
    String endpoint = "/api/getdoctors/idCity/specialityname";
    print(url + endpoint);
  }

  Future<void> getOrdersByAppointment(String appid, Function f1) async {
    String url = url_mdbook;
    String endpoint = "/api/order/appid";
    print(url + endpoint);
  }

  Future<void> getAllMedicalHistory(String appid, Function f1) async {
    String url = url_mdbook;
    String endpoint = "/api/history/appid";
    print(url + endpoint);
  }

  Future<void> getAllDoctors(Function f1) async {
    String url = url_mdbook;
    String endpoint = "/api/doctors";
    print(url + endpoint);
  }

  Future<void> getAppointment(String appid, Function f1) async {
    String url = url_mdbook;
    String endpoint = "/api/Appointment/appid";
    print(url + endpoint);
  }

  Future<File> loadNetwork(String path) async {
    var url = Uri.parse(path);
    final response = await http.get(url);
    final bytes = response.bodyBytes;
    return _storeFile(path, bytes);
  }

  Future<File> _storeFile(String url, List<int> bytes) async {
    final filename = basename(url);
    final dir = await getApplicationDocumentsDirectory();

    final file = File('${dir.path}/$filename');
    await file.writeAsBytes(bytes, flush: true);
    return file;
  }
}
