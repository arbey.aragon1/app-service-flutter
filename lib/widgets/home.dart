
import 'package:flutter/material.dart';
import 'package:app_service/widgets/header.dart';
import 'package:app_service/widgets/tabs.dart';

import 'constants.dart';
import 'menu.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
 
  int _currentIndex = 0;
  final List<Map> menu = [
    {'text':'Citas Recientes', 'path':'/recent-appointments', 'icon':Icons.event_available},
    {'text':'Buscar Médico', 'path':'/doctor-serch', 'icon':Icons.saved_search},
    {'text':'Compartir Archivos', 'path':'/shared-data', 'icon':Icons.share},
    {'text':'Mis Archivos', 'path':'/file-list', 'icon':Icons.snippet_folder},
    {'text':'Mi Cuenta', 'path':'/profile', 'icon':Icons.account_box},
  ];
  _HomeState();
  @override
  Widget build(BuildContext context){

    return Scaffold(
        appBar: Header(),  
        body: new Column(
        children: <Widget>[
          getWelcomeHome(),
          getDay(),
          new Expanded(
            child: GridView.builder(
               padding: EdgeInsets.all(20.0),
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 200,
                childAspectRatio: 3 / 2,
                crossAxisSpacing: 20,
                mainAxisSpacing: 20),
            
            itemCount: menu.length,
            itemBuilder: (BuildContext ctx, index) {
              return getItemContainer(context, menu[index]['text'], menu[index]['path'], menu[index]['icon']);
            }
              ),

            ),
          
          ],
        ),
        drawer: Menu(),
        bottomNavigationBar: Tabs()
      );
  }


  Widget getWelcomeHome() {
    return Container(
                  margin: EdgeInsets.all(5),
                  padding: EdgeInsets.only(top: 40),
                  child: Text(
                    "Hola, Luisa te damos la Bienvenida",
                    style: TextStyle(
                      color: Colors.black, 
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      
                    ),
                  ),  
          );
  }

 Widget getDay() {
    return Container(
                  margin: EdgeInsets.all(5),
                  child: Text("Fecha"),    
          );
  }




  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  Widget getItemContainer(BuildContext context, String title, String path, IconData ico) {
    return Card(
          margin:  EdgeInsets.all(5.0),
          child: InkWell(                        
          child: Container(
            margin: EdgeInsets.all(5),
            padding: const EdgeInsets.all(5),
            alignment: Alignment.center,
            child: Column(
               mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(title, style: new TextStyle(fontSize: 12.0)),
                Icon(
                  ico,
                  size: 26.0,
                  color: COLOR_PRIMARY,
                )
              ]
            ),
          color: COLOR_LIGHT_GREY,
        ),                        
        onTap: () {                  
          Navigator.pushNamed(context, path);
          },                      
        ),
        );

  }



}

