import 'package:flutter/material.dart';
import 'package:app_service/widgets/header.dart';
import 'package:app_service/widgets/menu.dart';
import 'package:app_service/widgets/tabs.dart';
import 'dart:io';
import 'constants.dart';

class SharedData extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SharedDataState();
  }
}

class _SharedDataState extends State<SharedData> {
  final List<Map> appointments = [
    {
      'date': 'Fecha de solicitud 8 jun 2021',
      'name':'Dr. Jhon Mendez',
      'speciality':'MEDICINA GENERAL - CARDIOLOGIA',
      'company':'Company',
      'phone': '3118352833',
      'address': 'Calle 93 # 18 98',
      'detail': 'Torre2 - BOGOTA D.C. - USAQUEN'
    },
    {
      'date': 'Fecha de solicitud 8 jun 2021',
      'name':'Dr. Jhon Mendez',
      'speciality':'MEDICINA GENERAL - CARDIOLOGIA',
      'company':'Company',
      'phone': '3118352833',
      'address': 'Calle 93 # 18 98',
      'detail': 'Torre2 - BOGOTA D.C. - USAQUEN'
    },
  ];
  String? _doctor;
  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: Header(),
        body: Column(
            children: <Widget>[

              Padding(
                padding: EdgeInsets.all(16.0),
                child: Column(children: [
                  Center(child: Text("Buscar especialista y solicitar una cita", style: TextStyle(
                      fontSize: 25, fontWeight: FontWeight.w500
                  ))),
                  DropdownButton<String>(
                    isExpanded: true,
                    focusColor:Colors.white,
                    value: _doctor,
                    style: TextStyle(color: Colors.white),
                    iconEnabledColor:Colors.black,
                    items: <String>[
                      '--',
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value,style:TextStyle(color:Colors.black),),
                      );
                    }).toList(),
                    hint:Text(
                      "Seleccionar doctor",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.w500),
                    ),
                    onChanged: (String? value) {
                      if(value!= null){
                        setState(() {
                          _doctor = value;
                        });
                      }
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 40.0, left: 40.0, top: 40),
                    child: ConstrainedBox(
                      constraints: const BoxConstraints(minWidth: double.infinity),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: COLOR_PRIMARY,
                          padding: EdgeInsets.only(top: 12, bottom: 12),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              side: BorderSide(color: COLOR_PRIMARY)),
                        ),
                        child: Text(
                          'Compartir',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                        onPressed: () async {
                          //Navigator.pushNamed(context, '/doctor-list');
                          final String action = await _asyncConfirmDialog(context);
                        }
                      ),
                    ),
                  ),

                ],)
              ),
              ItemSharedList(appointment: appointments[0], btnActive: false),
              Text(
                'Compartiendo con: ',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Expanded(
                  child: ListView.builder(
                      itemCount: appointments.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ItemSharedList(appointment: appointments[index], btnActive: true);
                      }))
            ]),
        drawer: Menu(),
        bottomNavigationBar: Tabs()
    );
  }

  Future _asyncConfirmDialog(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Compartir'),
          content: const Text(
              '¿Está seguro de compartir sus archivos y su historia con :{{name}}? '),
          actions: [
            FlatButton(
              child: const Text('Cerrar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: const Text('Compartir'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }
}

class ItemSharedList extends StatelessWidget {
  ItemSharedList({required this.appointment, required this.btnActive});

  final Map appointment;
  final bool btnActive;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
          color: COLOR_LIGHT_GREY,
        ),
        width: double.infinity,
        height: 140,
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: Container(
                    width: 70,
                    height: 70,
                    margin: EdgeInsets.only(right: 15),
                    child: Image(image: AssetImage("assets/images/sharing-no-image.png"))),
              ),
              Expanded(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          appointment['name'],
                        ),
                        Text(
                          appointment['speciality'],
                        ),
                        Text(
                          appointment['detail'],
                        ),
                        Row(
                          children: <Widget>[
                            Icon(Icons.call,size: 20,),
                            SizedBox(width: 5,),
                            Text(appointment['phone'],),
                          ],
                        ),
                        if (btnActive) ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: COLOR_PRIMARY
                          ),
                          child: Text(
                            'Dejar de compartir',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          onPressed: () async {
                            //Navigator.pushNamed(context, '/request-appointment');
                          },
                        ),
                      ])
              )
            ]
        ));
  }

}