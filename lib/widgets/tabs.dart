
import 'package:flutter/material.dart';
import 'package:app_service/widgets/header.dart';

import 'constants.dart';
import 'menu.dart';

class Tabs extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TabsState();
  }
}

class _TabsState extends State<Tabs> {
  int _currentIndex = 0;
  
  _TabsState();
  @override
  Widget build(BuildContext context){

    return BottomNavigationBar(
          selectedItemColor: COLOR_PRIMARY,
          onTap: onTabTapped,
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Inicio'),
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.event_available),
                title: Text('Citas')
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.medical_services),
              title: Text('Médico'),
            ),
          ],
        );
  }

  void onTabTapped(int index) {
    if(index == 0){
      Navigator.pushNamed(context, '/home');
    } else if(index == 1){
      Navigator.pushNamed(context, '/appointments-progress');
    } else if(index == 2){
      Navigator.pushNamed(context, '/doctor-serch');
    }
    setState(() {
      _currentIndex = index;
    });
  }

}

