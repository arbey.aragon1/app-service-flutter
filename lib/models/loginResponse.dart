import 'dart:convert';
import 'package:dio/dio.dart';

class LoginResponse {
  LoginResponse({this.token, this.refresh_token});

  String? token;
  String? refresh_token;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    token: json["token"],
    refresh_token: json["refresh_token"]
  );

  Map<String, dynamic> toJson()=>{
    "token": this.token,
    "refresh_token": this.refresh_token
  };
}