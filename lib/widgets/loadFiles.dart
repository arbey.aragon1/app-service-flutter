import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:app_service/widgets/header.dart';
import 'package:app_service/widgets/menu.dart';
import 'package:app_service/widgets/tabs.dart';

import 'constants.dart';

class LoadFiles extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoadFilesState();
  }
}

class _LoadFilesState extends State<LoadFiles> {
  //FilePickerResult? selectedfile;
  String _selectedDate = 'Tap to select date';
  DateTime currentDate = DateTime.now();
  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate)
      setState(() {
        currentDate = pickedDate;
      });
  }

    selectFile() async {
     /*selectedfile = await FilePicker.platform.pickFiles(
          type: FileType.custom,
          allowedExtensions: ['jpg', 'pdf', 'mp4'],
     );*/
     setState((){}); //update the UI so that file name is shown
  }


  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: Header(),
        body: Container(
          padding: EdgeInsets.only(left: 16, top: 25, right: 16 ),
          child: ListView(
            children: [
              Text("Cargar archivos", style: TextStyle(
                fontSize: 25, fontWeight: FontWeight.w500
              )),
              SizedBox(height: 15,),
              TextField(
                decoration: InputDecoration(
                  labelText: "Titulo",
                  hintText: "Titulo"
                ),
              ),
              SizedBox(height: 15,),
              TextFormField(
                decoration: InputDecoration(
                    labelText: "Fecha del procedimiento*",
                    hintText: "Comentarios"
                ), controller: TextEditingController(text: DateFormat('yyyy-MM-dd').format(currentDate).toString()),
                onTap: () {
                  _selectDate(context);
                }
              ),
              SizedBox(height: 15,),
              TextFormField(
                  keyboardType: TextInputType.multiline,
                  maxLines: 3,
                  maxLength: 200,
                  decoration: InputDecoration(
                    labelText: "Comentarios",
                    hintText: "Comentarios"
                  ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 40.0, left: 40.0, top: 60),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: double.infinity),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: COLOR_PRIMARY,
                      padding: EdgeInsets.only(top: 12, bottom: 17),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(color: COLOR_PRIMARY)),
                    ),
                    child: Text(
                      'Selecciona archivo',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    onPressed: () {
                      selectFile();
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 40.0, left: 40.0, top: 15),
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: double.infinity),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: COLOR_PRIMARY,
                      padding: EdgeInsets.only(top: 12, bottom: 17),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(color: COLOR_PRIMARY_BLUE)),
                    ),
                    child: Text(
                      'Cargar archivo',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    onPressed: (){
                      Navigator.pushNamed(context, '/file-detail');
                    },
                  ),
                ),
              ),

            ]
          )
        ),
        drawer: Menu(),
        bottomNavigationBar: Tabs()
      );
  }

  uploadFile(){
    /*if(selectedfile != null) {
      String path;
      path = selectedfile!.files.single.path!;
      File file = File(path);
      print(path);

    } else {
      // User canceled the picker
    }*/
  }
}