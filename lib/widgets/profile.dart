import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_service/widgets/tabs.dart';
import 'package:intl/intl.dart';

import 'constants.dart';
import 'header.dart';
import 'menu.dart';


class Profile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProfileState();
  }
}

class _ProfileState extends State<Profile> {
  String? _chosenValueSex;
  String? _chosenValueDocType;
  String _selectedDate = 'Tap to select date';
  DateTime currentDate = DateTime.now();
  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate = await showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: DateTime(2015),
        lastDate: DateTime(2050));
    if (pickedDate != null && pickedDate != currentDate)
      setState(() {
        currentDate = pickedDate;
      });
  }


  @override
  Widget build(BuildContext context){

    return Scaffold(
        appBar: Header(),
        body: Container(
          padding: EdgeInsets.only(left: 16, top: 25, right: 16 ),
          child: SingleChildScrollView(

            child: Column(
              
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
              Text("Perfil de usuario", style: TextStyle(
                fontSize: 17, fontWeight: FontWeight.w500
              )),
              SizedBox(height: 15,),
              Center(child: 
                Stack(children: [
                  Container(
                    width: 130,
                    height: 130,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: new AssetImage("assets/images/sharing-no-image.png")
                      )
                    ),
                  )
                  ],
                ),
              ),
              SizedBox(height: 15,),
             
           Padding(
                padding: const EdgeInsets.only(right: 40.0, left: 40.0, top: 1),
                    child: Text(
                      'Cargar Imagen de Perfil',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Color.fromRGBO(14, 196, 173, 1.0),
                      ),
                    ), 
                    
                ),

          SizedBox(height: 25,),
          Align(
            
            child:  Row(
              
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 crossAxisAlignment: CrossAxisAlignment.center,
               
                children: <Widget>[
                  Flexible(  // see 3
                    child: Text(
                      "Género",
                      style: TextStyle(
                        color: Colors.black, 
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        
                      ),
                    ),  
                  ),
                  Flexible(  // see 3
                    child:  DropdownButton<String>(

                      isExpanded: false,
                      focusColor:Colors.white,
                      value: _chosenValueSex,
                      style: TextStyle(color: Colors.white),
                      iconEnabledColor:Colors.black,
                    
                      items: <String>[
                        'Masculino',
                        'Femenino',
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                      
                          value: value,
                          child: Text(value,style:TextStyle(color:Colors.black),),
                        );
                      }).toList(),
                 
                      onChanged: (String? value) {
                        if(value!= null){
                          setState(() {
                            _chosenValueSex = value;
                          });
                        }  
                      },
                    ),
                  ),
                  
                ],
              ),
          ),

         SizedBox(height: 15,),

         Align(
          
            child: Row(
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(  // see 3
                    child: Text(
                      "Tipo de Documento",
                      style: TextStyle(
                        color: Colors.black, 
                        fontSize: 15,
                        fontWeight: FontWeight.w400,  
                      ),
                    ),  
                  ),
                  Flexible(  // see 3
                    child: DropdownButton<String>(
                      
                      isExpanded: false,
                      focusColor:Colors.white,
                      value: _chosenValueDocType,
                      style: TextStyle(color: Colors.white),
                      iconEnabledColor:Colors.black,
                      items: <String>[
                        'TI',
                        'CC',
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value,style:TextStyle(color:Colors.black),),
                        );
                      }).toList(),
                    
                      onChanged: (String? value) {
                        if(value!= null){
                          setState(() {
                            _chosenValueDocType = value;
                          });
                        }
                      },
                    ),
                  ),
                  
                ],
              ),
          ),

          SizedBox(height: 15,),
              Container(
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Text('Ducumento',
                       style: TextStyle(
                        color: Colors.black, 
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      
                          ),
                      ),
                    ),
                    Expanded(
                      child:  TextField(
                      obscureText: false,
                     decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color.fromRGBO(14, 196, 173, 1.0), width: 2.0),
                          ),
                          border: OutlineInputBorder(),
                         
                      ),
                    ),
                    ),
                ],
              ),
              ),
    
              SizedBox(height: 15,),
              Center(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Expanded(
                      child: Text('Fecha de Nacimiento',
                       style: TextStyle(
                        color: Colors.black, 
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      
                          ),
                      ),
                    ),

                    Expanded(
                      child: TextFormField(
                          obscureText: false,
                          decoration: new InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color.fromRGBO(14, 196, 173, 1.0), width: 2.0),
                            ),
                            border: OutlineInputBorder(),
                          ),
                          controller: TextEditingController(text: DateFormat('yyyy-MM-dd').format(currentDate).toString()),
                          onTap: () {
                            _selectDate(context);
                          }
                      )),
                  ],
                ),
              ),

            SizedBox(height: 15,),
            Container(
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Text('Primer Nombre',
                       style: TextStyle(
                        color: Colors.black, 
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      
                          ),
                      ),
                    ),
                    Expanded(
                      child:  TextField(
                      obscureText: false,
                      decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color.fromRGBO(14, 196, 173, 1.0), width: 2.0),
                          ),
                          border: OutlineInputBorder(),
                         
                      ),
                    ),
                    ),
                ],
              ),
              ),
              SizedBox(height: 15,),
              Container(
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Text('Segundo Nombre',
                       style: TextStyle(
                        color: Colors.black, 
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      
                          ),
                      ),
                    ),
                    Expanded(
                      child:  TextField(
                      obscureText: false,
                      decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color.fromRGBO(14, 196, 173, 1.0), width: 2.0),
                          ),
                          border: OutlineInputBorder(),
                         
                      ),
                    ),
                    ),
                ],
              ),
              ),
              SizedBox(height: 15,),
              Container(
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Text('Primer Apellido',
                       style: TextStyle(
                        color: Colors.black, 
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      
                          ),
                      ),
                    ),
                    Expanded(
                      child:  TextField(
                      obscureText: false,
                     decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color.fromRGBO(14, 196, 173, 1.0), width: 2.0),
                          ),
                          border: OutlineInputBorder(),
                         
                      ),
                    ),
                    ),
                ],
              ),
              ),
              SizedBox(height: 15,),
              Container(
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Text('Segundo Apellido',
                       style: TextStyle(
                        color: Colors.black, 
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      
                          ),
                      ),
                    ),
                    Expanded(
                      child:  TextField(
                      obscureText: false,
                      decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color.fromRGBO(14, 196, 173, 1.0), width: 2.0),
                          ),
                          border: OutlineInputBorder(),
                         
                      ),
                    ),
                    ),
                ],
              ),
              ),
              SizedBox(height: 15,),
              Container(
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Text('Email',
                       style: TextStyle(
                        color: Colors.black, 
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      
                          ),
                      ),
                    ),
                    Expanded(
                      child:  TextField(
                      obscureText: false,
                      decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color.fromRGBO(14, 196, 173, 1.0), width: 2.0),
                          ),
                          border: OutlineInputBorder(),
                         
                      ),
                    ),
                    ),
                ],
              ),
              ),
              SizedBox(height: 15,),
              Container(
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Text('Celular',
                       style: TextStyle(
                        color: Colors.black, 
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      
                          ),
                      ),
                    ),
                    Expanded(
                      child:  TextField(
                      obscureText: false,
                      decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color.fromRGBO(14, 196, 173, 1.0), width: 2.0),
                          ),
                          border: OutlineInputBorder(),
                         
                      ),
                    ),
                    ),
                ],
              ),
              ),
               Padding(
              padding: const EdgeInsets.only(right: 3.0, left: 3.0, top: 15),
              child: ConstrainedBox(
                constraints: const BoxConstraints(minWidth: double.infinity),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary:  COLOR_PRIMARY,
                    padding: EdgeInsets.only(top: 12, bottom: 17),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: BorderSide(color: COLOR_PRIMARY)),
                  ),
                  child: Text(
                    'Guardar',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: (){
                      Navigator.pushNamed(context, '/home');
                  },
                ),
              ),
            ),
              
            ],

            ),
   




          )
        ),
        drawer: Menu(),
        bottomNavigationBar: Tabs()
      );
  }
}

