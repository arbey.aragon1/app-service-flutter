import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_service/widgets/tabs.dart';

import 'constants.dart';
import 'header.dart';
import 'menu.dart';


class RecentAppointments extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _RecentAppointmentsState();
  }
}


class _RecentAppointmentsState extends State<RecentAppointments> {
  final List<Map> appointments = [
    {
      'date': '19 Feb. 2021',
      'name':'Dr. Jhon Mendez',
      'speciality':'MEDICINA GENERAL - CARDIOLOGIA',
      'company':'Company',
      'phone': '3118352833',
      'address': 'Calle 93 # 18 98',
      'detail': 'Torre2 - BOGOTA D.C. - USAQUEN',
      'status': 'Consulta de primera vez'
    },
    {
      'date': '19 Feb. 2021',
      'name':'Dr. Jhon Mendez',
      'speciality':'MEDICINA GENERAL - CARDIOLOGIA',
      'company':'Company',
      'phone': '3118352833',
      'address': 'Calle 93 # 18 98',
      'detail': 'Torre2 - BOGOTA D.C. - USAQUEN',
      'status': 'Consulta de primera vez'
    },
  ];

  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: Header(),
        body: DefaultTabController(
        length: 2,
        child: Column(
          children: <Widget>[
            Container(
              constraints: BoxConstraints.expand(height: 50),
              child: TabBar(
                labelColor: COLOR_PRIMARY,
                indicatorColor: COLOR_PRIMARY,
                tabs: [
                Tab(text: "Citas recientes"),
                Tab(text: "Citas programadas"),
              ]),
            ),
            Expanded(
              child: Container(
                child: TabBarView(children: [
                  getList(),
                  getList(),
                ]),
              ),
            )
          ],
        ),
      ),
        drawer: Menu(),
        bottomNavigationBar: Tabs()
      );
  }


  Widget getList() {
    return Container(
      child: ListView.builder(
        itemCount: appointments.length,
        itemBuilder: (context, index) {
          return Column(
            children: <Widget>[
              getItem(index),
              Divider(),
            ],
          );
        },
      ),
    );
  }

  Widget getItem(int index){
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
          color: COLOR_LIGHT_GREY,
        ),
        width: double.infinity,
        height: 150,
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: Container(
                    width: 70,
                    height: 70,
                    margin: EdgeInsets.only(right: 15),
                    child: Image(image: AssetImage("assets/images/sharing-no-image.png"))),
              ),
              Expanded(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(appointments[index]['date'],),
                        Text(appointments[index]['name'],),
                        SizedBox(height: 5,),
                        Text(
                          appointments[index]['status'],
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 10,
                              background: Paint()
                                ..strokeWidth = 14.0
                                ..color = COLOR_PRIMARY
                                ..style = PaintingStyle.stroke
                                ..strokeJoin = StrokeJoin.round),
                        ),
                        SizedBox(height: 5,),
                        RaisedButton.icon(
                          onPressed: (){ print('Button Clicked.'); },
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(Radius.circular(5.0))),
                          label: Text('0',
                            style: TextStyle(color: Colors.black),),
                          icon: Icon(Icons.favorite_border, color:Colors.black,),
                          textColor: Colors.black,
                          color: COLOR_LIGHT_GREY,),
                      ])
              ),
              getMenuButton(),
            ]
        ));
  }


 Widget getMenuButton() {
    return PopupMenuButton(
          itemBuilder: (context) {
            var list = <PopupMenuEntry<Object>>[
              PopupMenuItem(
                value: 1,
                child: Text("Ver detalles"),
              ),
            ];
            return list;
          },
          onSelected: (value) {
            print("value:$value");
            Navigator.pushNamed(context, '/appointment-detail');
          },
          icon: Icon(
            Icons.more_vert,
            size: 30,
            color: Colors.black,
          ),
        );
  }
}

